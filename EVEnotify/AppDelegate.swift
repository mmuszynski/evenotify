//
//  AppDelegate.swift
//  EVEnotify
//
//  Created by Mike Muszynski on 11/6/14.
//  Copyright (c) 2014 Mike Muszynski. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    //71a6233cfb1932991111197cb9c272aceba5af9b

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.'
        var notificationTypes = UIUserNotificationType.Badge | UIUserNotificationType.Alert | UIUserNotificationType.Alert
        var settings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        application.registerForRemoteNotifications()
        
        var defaultAction = UIMutableUserNotificationAction()
        defaultAction.identifier = "DEFAULT_NOTIFICATION"
        defaultAction.title = "Okay"
        defaultAction.activationMode = UIUserNotificationActivationMode.Background
        defaultAction.destructive = false
        defaultAction.authenticationRequired = false
        
        var defaultCategory = UIMutableUserNotificationCategory()
        defaultCategory.identifier = "DEFAULT_NOTIFICATION_CATEGORY"
        defaultCategory.setActions([defaultAction], forContext: UIUserNotificationActionContext.Default)
        
        var defaultSettings = UIUserNotificationSettings(forTypes: UIUserNotificationType.Alert, categories: NSSet(object: defaultCategory))
        UIApplication.sharedApplication().registerUserNotificationSettings(defaultSettings)

        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        var x = [UInt8](count: deviceToken.length, repeatedValue: 0)
        var string = ""
        var testData = NSData(bytes: deviceToken.bytes, length: deviceToken.length)
        var byteArray = [UInt8](count: deviceToken.length, repeatedValue: 0)
        deviceToken.getBytes(&byteArray, length: deviceToken.length)
        
        var hexBits = "" as String
        for value in byteArray {
            hexBits += NSString(format:"%2X", value) as String
        }
        
        let hexBytes = hexBits.stringByReplacingOccurrencesOfString("\u{0020}", withString: "0", options: NSStringCompareOptions.CaseInsensitiveSearch)
        
        NSUserDefaults.standardUserDefaults().setObject(hexBytes, forKey: "deviceToken")
        println(NSUserDefaults.standardUserDefaults().objectForKey("deviceToken"))
        
    }


}

