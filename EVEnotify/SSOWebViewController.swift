//
//  SSOWebViewController.swift
//  EVEnotify
//
//  Created by Mike Muszynski on 11/6/14.
//  Copyright (c) 2014 Mike Muszynski. All rights reserved.
//

import UIKit

class SSOWebViewController: UIViewController, UIWebViewDelegate, NSURLConnectionDataDelegate {
    
    @IBOutlet weak var ssoWebView: UIWebView?
    var rawJSON : NSMutableData?

    override func viewDidLoad() {
        super.viewDidLoad()
        ssoWebView?.delegate = self
        
        let deviceToken = NSUserDefaults.standardUserDefaults().objectForKey("deviceToken") as String

        var request = NSMutableURLRequest(URL: NSURL(string: "https://login.eveonline.com/oauth/authorize?response_type=code&redirect_uri=http://everelay.net/auth/eve/&client_id=6313192e5a2f49aeb043358181b2154a&scope=&state=\(deviceToken)_71a6233cfb1932991111197cb9c272aceba5af9b")!)
        
        //
        
        ssoWebView?.loadRequest(request)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        
        if request.URL.host == "everelay.net" {
            println("not loading")
            var downloader = NSURLConnection(request: request, delegate: self)!
            downloader.start()
            
            return false
        }
        return true
        
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
    }
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        rawJSON = NSMutableData()
        
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        rawJSON?.appendData(data)
        
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
        var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(rawJSON!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
